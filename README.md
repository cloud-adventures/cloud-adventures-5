# Cloud Computing 5

<figure align = "center">
<img src="images/banner5.png" alt="Trulli" width="800" height="423"">
</figure>

## Name
Creating a EKS Cluster with Terraform modules. 

## Description
Project takes ideas from online courses, websites and articles to produce a working simple application:
- Creates AWS Architecture in Terraform using modules
- Creates EKS cluster and relevant policies and permissions with Terraform  
- Loadbalances pods running in EKS

## AWS Architecture

<figure align = "center">
<img src="images/cloud5.png" alt="AWS " width="800" height="841"">
<figcaption align = "center"><b>AWS Architecture</b></figcaption>
</figure>


## Key Folders and files
- `k8s` - various yaml files for ingress and and deploying niginx images on pods 
- `modules` - contains eks and network terraform files in child module form 
- `main.tf output.tf provider.tf` - terraform root module

## Usage
- Run terraform:\
    `terraform apply`
- Export cluster:\
`aws eks --region us-east-1 update-kubeconfig --name my-cluster`
- Deploy public load balancer and service objects:\
`kubectl apply -f k8s/deployment.yaml`\
 `kubectl apply -f k8s/public-lb.yaml`
- Get public IP address form loadbalancer:\
`kubectl get all -o wide`\
Navigate to that IP address on your browser where NIGINX landing page will be present.


## Contributing
- [EKS terraform](https://antonputra.com/terraform/how-to-create-eks-cluster-using-terraform/#create-public-load-balancer-on-eks)
- [EKS permissions](https://marcincuber.medium.com/amazon-eks-with-oidc-provider-iam-roles-for-kubernetes-services-accounts-59015d15cb0c)
- [Provision an EKS Cluster HasiCorp](https://learn.hashicorp.com/tutorials/terraform/eks)

## Project status
Project is ongoing:
- utilise CI/CD pipelines
