terraform {
  backend "s3" {
    bucket         = "aws-terraform-3-state-tf"
    key            = "aws-terraform-3.tfstate"
    region         = "us-east-1"
    dynamodb_table = "aws-terraform-3-tf-state-lock"
    encrypt        = true
  }

  required_version = "~> v1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
provider "aws" {
  region  = "us-east-1"
  profile = "default"
}
